#!/bin/sh

AD_FILTER=$(echo $AD_FILTER | sed  's`\\`\\\\`g' | sed  's`&`\\&`g' | sed 's,`,\\`,g')
BIND_PASSWORD=$(echo $BIND_PASSWORD | sed  's`\\`\\\\`g' | sed  's`&`\\&`g' | sed 's,`,\\`,g')

sed -i "s\`\${MLFLOW_URL}\`$MLFLOW_URL\`" /etc/nginx/conf.d/default.conf
sed -i "s\`\${AD_FILTER}\`$AD_FILTER\`" /etc/nginx/conf.d/default.conf
sed -i "s\`\${BASE_DN}\`$BASE_DN\`" /etc/nginx/conf.d/default.conf
sed -i "s\`\${BIND_USER}\`$BIND_USER\`" /etc/nginx/conf.d/default.conf
sed -i "s\`\${BIND_PASSWORD}\`$BIND_PASSWORD\`" /etc/nginx/conf.d/default.conf
sed -i "s\`\${LDAP_SERVER}\`$LDAP_SERVER\`" /etc/nginx/conf.d/default.conf

nginx -g 'daemon off;'
